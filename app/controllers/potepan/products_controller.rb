module Potepan
  class ProductsController < ApplicationController
    def show
      @product = Spree::Product.friendly.find(params[:id])
    end
  end
end
