module ApplicationHelper
  def full_title(title = "")
    base_title = "BIGBAG Store"
    base_title.freeze
    if title.empty?
      base_title
    else
      "#{title} - #{base_title}"
    end
  end
end
