require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET / show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:image) { create(:image) }
    let(:product1) { create(:product, name: "product1", taxons: [taxon]) }
    let(:product2) { create(:product, name: "product2", price: 100) }

    before do
      product1.images << image
      get potepan_category_path(taxon.id)
    end

    example "カテゴリー詳細ページに移動できる" do
      expect(response.status).to eq 200
    end

    example "カテゴリーが取得できている" do
      expect(response.body).to include taxonomy.name
    end

    example "カテゴリー名が取得できている" do
      expect(response.body).to include taxon.name
    end

    context "カテゴリー名(taxon.name)に属している商品の場合" do
      example "商品名が取得できる" do
        expect(response.body).to include product1.name
      end

      example "値段が取得できている" do
        expect(response.body).to include product1.display_price.to_s
      end
    end

    context "カテゴリー名(taxon.name)に属していない商品の場合" do
      example "商品名が取得できない" do
        expect(response.body).not_to include product2.name
      end

      example "値段が取得できない" do
        expect(response.body).not_to include product2.display_price.to_s
      end
    end
  end
end
