require 'rails_helper'

RSpec.describe "Potepan::Products", :type => :request do
  describe "GET / show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    example "商品詳細ページに移動できる" do
      expect(response.status).to eq 200
    end

    example "商品名が取得できている" do
      expect(response.body).to include product.name
    end

    example "商品の値段が取得できている" do
      expect(response.body).to include product.display_price.to_s
    end

    example "商品についての説明が取得できている" do
      expect(response.body).to include product.description
    end

    example "商品の属するカテゴリー一覧ページへのリンクが取得できている" do
      expect(response.body).to include potepan_category_path(taxon.id)
    end
  end
end
