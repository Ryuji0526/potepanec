require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    context "引数を渡す時" do
      it "引数の値が返り値に反映される" do
        test_title = "TEST"
        expect(helper.full_title(test_title)).to eq("#{test_title} - BIGBAG Store")
      end
    end

    context "引数を渡さない時" do
      it "base_titleの値が返り値となる。" do
        expect(helper.full_title).to eq("BIGBAG Store")
      end
    end
  end
end
