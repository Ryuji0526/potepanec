require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:bag) { create(:taxon, name: "Bag", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:mug) { create(:taxon, name: "Mug", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product1) { create(:product, name: "bag", taxons: [bag]) }
  let!(:product2) { create(:product, name: "mug", price: 100, taxons: [mug]) }

  before { visit potepan_category_path(bag.id) }

  scenario "カテゴリー(bag)に属している商品が表示される。" do
    expect(page).to have_current_path potepan_category_path(bag.id)
    expect(page).to have_title "#{bag.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: bag.name
    expect(page).to have_selector ".pull-right li", text: bag.name
    within ".collapseItem" do
      expect(page).to have_content bag.name
      expect(page).to have_content mug.name
    end
    within ".productCaption" do
      expect(page).to have_selector "h5", text: product1.name
      expect(page).to have_selector "h3", text: product1.display_price
    end
  end

  scenario "指定したカテゴリーに属していない商品は表示されない。" do
    expect(page).not_to have_selector ".productCaption h5", text: product2.name
    expect(page).not_to have_selector ".productCaption h3", text: product2.display_price
  end

  scenario "別のカテゴリー詳細ページに移動できる" do
    click_link mug.name
    expect(page).to have_current_path potepan_category_path(mug.id)
    expect(page).to have_title "#{mug.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: mug.name
    expect(page).to have_selector ".pull-right li", text: mug.name
    within ".collapseItem" do
      expect(page).to have_content bag.name
      expect(page).to have_content mug.name
    end
    within ".productCaption" do
      expect(page).to have_selector "h5", text: product2.name
      expect(page).to have_selector "h3", text: product2.display_price
    end
  end

  scenario "商品詳細ページに移動できる" do
    click_link product1.name
    expect(page).to have_current_path potepan_product_path(product1.id)
    expect(page).to have_title "#{product1.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: product1.name
    expect(page).to have_selector ".pull-right li", text: product1.name
    within ".media-body" do
      expect(page).to have_selector "h2", text: product1.name
      expect(page).to have_selector "h3", text: product1.display_price
    end
    expect(page).to have_content product1.description
  end

  scenario "サイドバーの挙動(ドロップダウン)が正しく機能している", js: true do
    within ".sideBar" do
      expect(page).not_to have_content bag.name
      expect(page).not_to have_content mug.name
    end
    find(".navbar-nav a", text: taxonomy.name).click
    within ".sideBar" do
      expect(page).to have_content bag.name
      expect(page).to have_content mug.name
    end
  end
end
