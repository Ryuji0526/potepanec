require 'rails_helper'

RSpec.describe "Potepan::Products", type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let(:product) { create(:product, taxons: [taxon]) }

  before { visit potepan_product_path(product.id) }

  scenario "指定した商品の詳細が表示される。" do
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_selector '.page-title h2', text: product.name
    expect(page).to have_selector '.pull-right li', text: product.name
    within ".media-body" do
      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_selector 'h3', text: product.display_price
    end
    expect(page).to have_content product.description
  end

  scenario "商品が属するカテゴリーの詳細ページに移動できる" do
    click_link "一覧ページへ戻る"
    expect(page).to have_current_path potepan_category_path(taxon.id)
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: taxon.name
    expect(page).to have_selector ".pull-right li", text: taxon.name
    expect(page).to have_selector ".collapseItem li", text: taxon.name
    expect(page).to have_selector "h5", text: product.name
    expect(page).to have_selector "h3", text: product.display_price
  end
end
